'use strict';

angular.module('noiserApp')
  .config(function ($stateProvider) {
    $stateProvider
      .state('login', {
        url: '/l',
        templateUrl: 'app/login/login.html',
        controller: 'LoginCtrl'
      });
  });
